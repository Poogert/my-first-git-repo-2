#!/bin/bash

#Algorithm
#Greet user then ask for user input temp and remember it
#Output user inputted tempearture into the other temperatures. f ->C,K. C->f,k. k->f,C.
#Farenheit to Celsius formula (F-32) * (5/9) = C
#Farenheit to Kelvin formula (F-32) * (5/9) + 273.15 = K
#Celsius to Farenheit formula C(9/5) + 32 = F
#Celsius to Kelvin formula C + 273.15 = K
#Kelvin to Farenheit formula (K - 273.15) * 9/5 +32 = F
#Kelvin to Celcius formula K - 273.15 = C
#Output desired temps

#Pseudocode
#read user varnum
#read user F C or K vartemp
#if vartemp =  F then (varnum - 32) * (5/9) = varfc
#echo varfc
#also if F then (varF-32) * (5/9) +273.15 = varfk
#echo varfk
#also if C then varC(9/5) + 32 = varcf
#echo varcf
#also if C then varC + 273.15 = varck
#echo varck
#also if K then (varK - 273.15) * 9/5 + 32 = varkf
#echo varkf
#also if K then (varnum - 273.15) = varkc
#echo varkc

echo "Hello! Welcome to Poogerts Poogtastic Temperature converter! To continue please press Enter"

read -p "Please enter your temperature. " varnum
read -p "Please enter unit of measurement. Please enter F C or K. " vartemp

if [ $vartemp = F ]
then
	varfc=$(echo "($varnum - 32) * (5/9)" | bc -l)
	varfcround= printf "%.0f" $varfc
	echo $varfcround " degrees Celsius"

	varfk=$(echo "($varnum - 32) * (5/9) + 273.15" | bc -l )
	varfcround= printf "%.2f" $varfk
	echo $varfkround " Kelvins"

elif [ $vartemp = C ]
then
	varcf=$(echo " $varnum * (9/5) + 32 " | bc -l)
	varcfround= printf "%.0f" $varcf
	echo $varcfround " degrees Fahrenheit"

	varck=$(echo " $varnum + 273.15 " | bc -l)
	varckround= printf "%.2f" $varck
	echo $varckround " kelvins"
elif [ $vartemp = K ]
then
	varkf=$(echo "($varnum - 273.15) * (9/5) + 32" | bc -l)
	varkfround= printf "%.0f" $varkf
	echo $varkfround " degrees Fahrenheit"

	varkc=$(echo "$varnum - 273.15 " | bc -l)
	varkcround= printf "%.2f" $varkc
	echo $varkcround " Celsius"
else
	echo "Please enter a valid temp. F, C or K. (caps are important)"
fi
